import Vue from 'vue'
import VueRouter from 'vue-router'
import Intro from '../views/Intro'
import DetailsSelect from '../views/DetailsSelect'
import DetailsBotmoney from '../views/DetailsBotmoney'
import DetailsAx from '../views/DetailsAx'
import DetailsSpotter from '../views/DetailsSpotter'
import LandingPage from '../views/LandingPage'


Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'LandingPage',
    component: LandingPage
  },

  {
    path: '/video',
    name: 'Intro',
    component: Intro
  },
  {
    path: '/details/select',
    name: 'DetailsSelect',
    component: DetailsSelect
  },
  {
    path: '/details/botmoney',
    name: 'DetailsBotmoney',
    component: DetailsBotmoney
  },
  {
    path: '/details/ax-invest',
    name: 'DetailsAx',
    component: DetailsAx
  },
  {
    path: '/details/spotter',
    name: 'DetailsSpotter',
    component: DetailsSpotter
  },


];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router
